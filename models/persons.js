let persons = {
    person1:{
        firstName: 'Steven',
        lastName:'Gerrrard',
        age:35,
        id:1
    },
    person2:{
        firstName: 'Fernando',
        lastName:'Torres',
        age:37,
        id:2
    },
    person3:{
        firstName: 'Daniel',
        lastName:'Agger',
        age:41,
        id:3
    },
    person4:{
        firstName: 'Sami',
        lastName:'Hyypia',
        age:35,
        id:4
    }
    
}

module.exports = persons