const express = require('express');
const bodyParser = require('body-parser');
const person = require('./methods/persons');
const app = express();
app.use(bodyParser.json())


//gets the data from the database
app.get('/get-request', (req,res)=>{
    res.send(person)
});

//It posts a new contnet into the database
app.post('/post-request',async (req,res)=>{
   let newPerson  = await req.body;
   person[`person${newPerson.id}`]= newPerson;
   res.send(newPerson)

})


//this function below gets only one element as specified by the input
app.get('/getbyId/:id', async(req,res)=>{
  const  getbyId = await person[`person${req.params.id}`]
 res.send(getbyId);
})

//the delete function deletes the content  in the database
app.delete('/delete-post/:id', async(req,res)=>{
    const deleteUser = await person[`person${req.params.id}`]
    delete  person[`person${req.params.id}`];
    res.send(deleteUser)
})

//the put function updates the content which was already stored in the  database 
app.put('/update/:id', (req,res) =>{
    let updatePerson = req.body
    if (person[`person${req.params.id}`] != null){
        person[`person${req.params.id}`] = updatePerson
        res.send(updatePerson);

    }
    else{
        res.send(`No previously inserted a player with that name`)
    }
})

app.listen(3000, ()=>{
    console.log(`listning to port 3000`)
})